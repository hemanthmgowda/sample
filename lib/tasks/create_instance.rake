namespace :sample do
  desc "Create an EC2 instance"
  task :create_ec2_instance, [:region, :image_id, :min_count, :max_count, :instance_type, :tag_key, :tag_value] => [:environment] do |t, args|

    ec2 = Aws::EC2::Resource.new(region: args.region || ENV['AWS_REGION'] || 'us-east-1')

    instance = ec2.create_instances({image_id: args.image_id || ENV['IMAGE_ID'] || 'ami-066db1295a21665ff',
                                   min_count: args.min_count || ENV['min_count'] || 1,
                                   max_count: args.max_count || ENV['max_count'] || 1,
                                   instance_type: args.instance_type || ENV['instance_type'] || 't2.micro'})

    ec2.client.wait_until(:instance_running, {instance_ids: [instance.first.id]})

    instance.create_tags({ tags: [{ key: args.tag_key || ENV['tag_key'], value: args.tag_value || ENV['tag_value'] }]})

    puts '============================================'
    puts instance.first.id
    puts '============================================'
  end


  desc "Delete an EC2 instance"
  task :delete_ec2_instance, [:region, :instance_id] => [:environment] do |t, args|

    ec2 = Aws::EC2::Resource.new(region: args.region || ENV['AWS_REGION'] || 'us-east-1')
          
    instance = ec2.instance(args.instance_id)
        
    if instance.exists?
      case instance.state.code
      when 48  # terminated
        puts "#{id} is already terminated"
      else
        instance.terminate
      end
    end
  end

  desc "Create a RDS instance"
  task :create_rds_instance, [:region, :db_instance_class, :engine, :db_instance_identifier, :master_username, :master_user_password, :allocated_storage] => [:environment] do |t, args|

    rds = Aws::RDS::Resource.new(region: args.region || ENV['AWS_REGION'] || 'us-east-1')
          
    instance = rds.create_db_instance({ db_instance_identifier: args.db_instance_identifier || ENV['db_instance_identifier'] || 'mydbinstance',  
                                        db_instance_class: args.db_instance_class || ENV['db_instance_class'] || "db.t2.micro", 
                                        engine: args.engine || ENV['engine'] || "mysql",
                                        master_username: args.master_username || ENV['master_username'] || 'root',
                                        master_user_password: args.master_user_password || ENV['master_user_password'] || 'admin12345',
                                        allocated_storage: args.allocated_storage || ENV['allocated_storage'] || 20
                                      })

    puts '============================================'
    puts instance.id
    puts '============================================'
  end

end