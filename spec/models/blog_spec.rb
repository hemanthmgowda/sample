require 'rails_helper'

RSpec.describe Blog, type: :model do
  subject { Blog.new(title: 'New Sample Title', description: 'New Sample Description') }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without a title" do
      subject.title = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without an description" do
      subject.description = nil
      expect(subject).to_not be_valid
    end
  end
end
