# config valid for current version and patch releases of Capistrano
lock "~> 3.11.2"

set :application, "sample"
set :repo_url, "git@gitlab.com:hemanthmgowda/sample.git"

server '18.223.205.139', port: 22, roles: [:web, :app, :db], primary: true

set :user, 'ubuntu'
set :rvm_bin_path, "$HOME/bin"
set :rvm_ruby_version, 'ruby-2.6.3@sample'
set :pty, true
set :verbose, true
set :use_sudo, false
set :rails_env, "production"
set :stage, :production
set :deploy_via, :remote_cache
set :deploy_to, "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :ssh_options, { forward_agent: true, user: fetch(:user), keys: %w(~/Downloads/default.pem) }
set :branch, :master
set :puma_init_active_record, true
set :puma_state, "#{shared_path}/tmp/pids/puma.state"
set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"
set :puma_bind, "unix://#{shared_path}/tmp/sockets/puma.sock"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :puma_restart_command, "bundle exec puma -C #{shared_path}/puma.rb --daemon"

namespace :deploy do
	desc 'Initial Deploy'
	task :initial do
		on roles(:app) do
			before 'deploy:restart'
			invoke 'deploy'
		end
	end

	set :linked_files, %w{config/database.yml config/master.key config/credentials.yml.enc}
	append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets"

	after :finishing, :compile_assets
	after :finishing, :cleanup
	after :finishing, :restart
end
